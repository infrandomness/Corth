//
// Created by infrandomness on 22/07/22.
//

#ifndef CORTH_ARGUMENTS_HXX
#define CORTH_ARGUMENTS_HXX

#include "Project.hxx"

#include <cstring>
#include <filesystem>

class Arguments final {
public:
    /// Path to the forth source file.
    std::filesystem::path Path = ".";
    /// Arguments to dumb tokens or not.
    bool DumpTokens = false;
    /// Arguments to be verbose about the different compiler steps.
    bool Verbose = false;

    /// Constructor to create an instance of `Arguments`.
    /// This constructor's body calls the ParseArgs function.
    ///  \param Argc Argc from main.
    ///  \param Argv Argv from main.
    Arguments(int Argc, char **Argv);

private:
    int Argc;
    char **Argv;

    static std::string Usage(const std::string &BinaryName);

    /** Parses arguments from `Argv` and `Argc` variables.
     * \note This function is only meant to be called by the constructor.
     * \note The implementation of this function parses arguments backwards:
     * this avoids us from parsing the binary invocation part (which is a valid
     * path)
     * The parser is only able to parse single hyphen arguments; it also
     * expects the last argument to be a valid path to Forth file
     */
    void ParseArgs();
};

#endif // CORTH_ARGUMENTS_HXX
