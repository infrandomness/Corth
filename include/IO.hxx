//
// Created by infrandomness on 23/07/22.
//

#ifndef CORTH_IO_HXX
#define CORTH_IO_HXX

#include <filesystem>
#include <fstream>
#include <string>

class IO {
public:
    static std::string ReadFile(const std::filesystem::path &Path);
};

#endif // CORTH_IO_HXX
