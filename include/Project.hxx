//
// Created by infrandomness on 27/07/22.
//

#ifndef CORTH_PROJECT_HXX
#define CORTH_PROJECT_HXX

#include <filesystem>

class Project {
public:
    std::filesystem::path ProjectPath;
    explicit Project(const std::filesystem::path &ProjectPath);
    Project();
};

#endif // CORTH_PROJECT_HXX
