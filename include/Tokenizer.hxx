//
// Created by infrandomness on 22/07/22.
//

#ifndef CORTH_TOKENIZER_HXX
#define CORTH_TOKENIZER_HXX

#include <array>
#include <string_view>
#include <vector>

class Tokenizer {
public:
    enum TokenType {
        COLON,
        SEMICOLON,
        WORD,
        STRING,
        NUMBER,
        COMMENT,
        SLASH,
        STAR,
        PLUS,
        MINUS,
        NONE
    };

    static constexpr std::array<std::string_view, 11> TokenTypeArray{
        "COLON", "SEMICOLON", "WORD", "STRING", "NUMBER", "COMMENT",
        "SLASH", "STAR",      "PLUS", "MINUS",  "NONE"};

    static std::vector<TokenType> Tokenize(std::string_view Source);
};

#endif // CORTH_TOKENIZER_HXX
