//
// Created by infrandomness on 22/07/22.
//

#include "Arguments.hxx"

#include "Project.hxx"

#include <iostream>

Arguments::Arguments(int Argc, char **Argv) : Argc{Argc}, Argv{Argv} {
    ParseArgs();
}

void Arguments::ParseArgs() {
    for (int i = (Argc - 1); i >= 1; i--) {
        if (Argv[i][0] == '-')
            switch (Argv[i][1]) {
            case 'v':
                Verbose = true;
                break;
            case 'd':
                DumpTokens = true;
                break;
            case 'h':
                std::cout << Usage(Argv[0]);
                exit(EXIT_SUCCESS);
            default:
                break;
            }
        else if (std::filesystem::exists(Argv[i]))
            Path = std::filesystem::path(Argv[i]);
        else
            std::cout << Usage(Argv[0]);
    }
}

std::string Arguments::Usage(const std::string &BinaryName) {
    return "Usage: " + BinaryName + " [-h] [-d] [-v] path\n";
}

