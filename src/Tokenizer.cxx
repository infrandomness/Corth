//
// Created by infrandomness on 22/07/22.
//

#include "Tokenizer.hxx"

#include <cctype>
#include <cstring>
#include <iostream>
#include <string_view>
#include <vector>

std::vector<Tokenizer::TokenType> Tokenizer::Tokenize(std::string_view Source) {
    auto tokens = std::vector<Tokenizer::TokenType>();

    for (auto character = Source.begin(), end = Source.end(); character != end;
         character++) {
        switch (*character) {
        case '\\':
            while (*character != '\n')
                character++;
            break;
        case ',':
            tokens.push_back(TokenType::SEMICOLON);
            break;
        case ';':
            tokens.push_back(TokenType::COLON);
            break;
        case '/':
            tokens.push_back(TokenType::SLASH);
            break;
        case '*':
            tokens.push_back(TokenType::STAR);
            break;
        case '+':
            tokens.push_back(TokenType::PLUS);
            break;
        case '-':
            tokens.push_back(TokenType::MINUS);
            break;
        case ' ':
            break;
        default:
            // All the rest falls through
            break;
        }
    }
    return tokens;
}
