#include "Arguments.hxx"
#include "IO.hxx"
#include "Tokenizer.hxx"

#include <iostream>

int main(int argc, char **argv) {
    auto args = Arguments(argc, argv);
    auto project = Project(args.Path);
    auto tokens = Tokenizer::Tokenize(IO::ReadFile(args.Path));
    if (args.DumpTokens) {
        std::cout << "Tokens :\n";
        for (const auto &item : tokens)
            std::cout << Tokenizer::TokenTypeArray[item] << '\n';
    }

    return 0;
}
