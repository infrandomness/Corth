//
// Created by infrandomness on 23/07/22.
//

#include "IO.hxx"

#include <filesystem>
#include <iostream>
#include <iterator>
#include <string>

std::string IO::ReadFile(const std::filesystem::path &Path) {
    std::ifstream stream(Path);
    if (!stream.is_open()) {
        std::cout << "Fatal: Failed to open " << Path.string() << '\n';
        exit(EXIT_FAILURE);
    }

    return std::string((std::istreambuf_iterator<char>(stream)),
                       std::istreambuf_iterator<char>());
}
